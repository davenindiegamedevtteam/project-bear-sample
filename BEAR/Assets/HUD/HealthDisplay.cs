﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthDisplay : MonoBehaviour {





	public float amount;
	public Text text;

	private static bool dead;

	[Tooltip("If this AI is dead, no auto-load of scene.")]
	public bool isBot = false;
	private bool isBotDead;

	public Transform bar1;
	public Transform bar2;

	public float delay = 0.10f;
	private float total, x;




	void Start () {

		total = amount;
		x = 1;

		dead = false;
		isBotDead = false;

	}





	void FixedUpdate () {

		if (amount < 0.99f) {

			if (!dead && !isBotDead) {
			
				try {

					GetComponent<Animator> ().SetBool ("IsDead", true);

				} catch(MissingComponentException) {
				
				}

				// Resizing the collider boundary that matches the sprite minus the transparent background.
				gameObject.GetComponent<BoxCollider2D> ().offset = new Vector2 (0, 0);
				gameObject.GetComponent<BoxCollider2D> ().size = new Vector2 (0.83f, 0.19f);

			}

			amount = 0;

			if (!isBot) {
			
				dead = true;

			} else {
			
				isBotDead = true;

			}

		}

		try {

			text.text = "Health: " + amount;
//			text.text = "Health: " + ((amount/total) * 100) + "%";
			UpdateHealthbar ();

		} catch(UnassignedReferenceException) {
		
		} catch(NullReferenceException) {
		
		}
		
	}





	public void Heal (float amount) {

		float excess = amount + this.amount;
	
		if (excess > total) {
		
			this.amount = total;

		} else {

			this.amount += amount;

		}

	}





	public void UpdateHealthbar () {

		if (x > (amount / total)) {
		
			x -= delay;

		} else {
		
			x = amount / total;

		}
	
		bar1.localScale = new Vector3 (amount/total, 1, 1);
		bar2.localScale = new Vector3 (x, 1, 1);

	}





	public static bool IsDead() {
	
		return dead;

	}





}
