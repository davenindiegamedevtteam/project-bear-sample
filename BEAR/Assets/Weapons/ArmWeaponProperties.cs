using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[System.Serializable]
public class TimeProperties {

	public float firingInterval = 0;
	public float startFire = 0;
	public float endFire = 0;
	public float reloadTime = 0;
	public int burstCount = 0;

}





[System.Serializable]
public class PhysicsProperties {

	// Generator behavior on weapon
	public float energyCost = 0; // --> Required amount of energy needed in order to operate it.
	public float usageCost = 0; // --> Energy consumed per shot, usually energy based weapon.

	// Lock-on behavior
	public float range = 0; // --> In kilometers; the lockon range limit.
	public bool enableLockOn = true;
	public float lockboxW = 0;
	public float lockboxH = 0;
	public float recoil = 0; // --> In percentage.

}





[System.Serializable]
public class SpawnBulletProperties {

	public GameObject[] ammo;
	public Transform startPoint; // --> From your end tip of the gun's barrel or muzzle. 
	public GameObject shotDirection;
	public GameObject muzzleFlash; // --> Requires to include the ExplodeDuration script to take muzzle flash in effect to work.

	public float shotOffsetX;
	public float shotOffsetY;
	public float shotsPerBatch = 0; // --> For performing burst shot.

	public bool enableFiring = true;

}





[System.Serializable]
public class ArmSounds {

	public AudioSource source;
	public AudioClip reloadSound;
	public AudioClip ammoOutSound;
	public AudioClip punchSound;

}





[System.Serializable]
public class HitCollisionSetup {

	public bool enableHitCollision = false;
	public float impact = 0;
	public float damage = 0;
	public string targetName = "";

	[Tooltip("Name of attack animation.")]
	public string attackName = "";
	public Animator animator;

	public int scoreCost = 50;

	[Tooltip("AI will not move/follow target while attacking. Very useful in accordance to animation.")]
	public bool stopFollow = true;

	public bool setAsPit = false;

}





/**
 * 
 * This is intended for the arm (mostly rifles in any variant) and back weapon (cannons)
 * to manipulate for its behavior, destructive performance, and aiming capability.
 * 
 */
public class ArmWeaponProperties : MonoBehaviour {


	// Subproperties of this gun
	public TimeProperties time;
	public PhysicsProperties physics;
	public SpawnBulletProperties spawnProperties;
//	public MechProperties mech;

	// Bullet properties
	public int noOfRounds = 180;
	public int roundsPerMag = 30;
	private int fired;
	private int mag;
	private int direction = 1; // --> Left or right?
	public float minAccuracy = 100; // --> In percentage
	public float maxAccuracy = 100; // --> WARNING: Max accuracy should always be set higher than the min
	public float averageAccuracyAngle = 0; // --> The average angle of the shot, where 0 degrees is considered 100%. Recommended for shotguns. Set up between 5 to 10 degrees for rifles and machineguns.
	private float minAngle;
	private float maxAngle;
	private float finalAccuracyAngle; // --> Accuracy angle nultiply by direction value
	private float reload;

	// SFX of the Gun
//	public FireSoundController fireSfx;
	public ArmSounds behaviorSfx;

	// Firing Behavior Type
	public string firingMode = "automatic";
	public string weaponLockType = "SPECIAL";

	// UI Text Labels
	public string ammo;

	// For mouse click...
	public int mouseBtnIndex = 0;

	// Misc
	private GameObject yourProjectile;
	public bool showDebugLog = false;
	private bool isReloading = false;

	// Hit Collision Setup
	[Tooltip("This setup allows you to hit a target with just a contact AoE.")]
	public HitCollisionSetup hitCollision;
	public Move move;





	void Start () {

		if (hitCollision.impact < 0) {
		
			hitCollision.impact *= -1;

		}

		// Convert percentage into decimal value range between 0.00 and 1.00 by dividing it by base accuracy 100.
		minAccuracy = minAccuracy / 100;
		maxAccuracy = maxAccuracy / 100;

		// Transferring mag counter...
		if (time.reloadTime == 0) {

			// If this gun doesn't need reloading at all (especially belt-fed machinegun)
			// and this gun starts with 1 "mag" contain no of rounds.
			mag = noOfRounds;
			noOfRounds = 0;
			
		} else {
		
			mag = roundsPerMag;
			reload = time.reloadTime;

		}

		fired = 0;

	}





	void Update () {

//		if (!ShipBehavior.IsShipDestroyed ()) {
//		
//			KeyboardButtons ();
//
//		}

//		KeyboardButtons ();

		// Ammo Count
		if (isReloading) {

			ammo = "RELOADING";
			
		} else {
		
			ammo = mag + " / " + noOfRounds;

		}
	
	}





	void FixedUpdate() {

		// Run timer.
		time.startFire = Time.time;

		// Check for hit contacts...
		if(hitCollision.enableHitCollision) {

			return;

		}
	
		// Placed it here for touch buttons.
		bool isTriggerPulled = false;

		// Check the type of firing.
		if(firingMode == "automatic") {

			isTriggerPulled = CrossPlatformInputManager.GetButton("Fire");

		} else if((firingMode == "semi-automatic") || (firingMode == "lock-on")) {

			isTriggerPulled = CrossPlatformInputManager.GetButtonDown("Fire");

		}

		// Check if a player tapped FIRE button.
//		if(isTriggerPulled && !ShipBehavior.IsShipDestroyed ()) {
//
//			PullTrigger ();
//
//		}

		// Tell if input is on keyboard and mouse or from touchscreen. Will keep on firing before reloading.
//		if(!mech.defenses.isDestroyed) 
		try {

			FireControlEvent (isTriggerPulled);
			MagCheck ();

		} catch (System.NullReferenceException) {
		
		}

	}





	void KeyboardButtons(){

		if (firingMode == "automatic") {
		
			if (Input.GetKey (KeyCode.J) || Input.GetMouseButton(mouseBtnIndex)) {

				PullTrigger ();

			}

		} else if (firingMode == "semi-automatic") {
		
			if (Input.GetKeyDown (KeyCode.J) || Input.GetMouseButtonDown(mouseBtnIndex)) {

				PullTrigger ();

			}

		} else if (firingMode == "auto-fire") {

			PullTrigger ();

		} else if (firingMode == "lock-on") {

			if (Input.GetKeyDown (KeyCode.J) || Input.GetMouseButtonDown(mouseBtnIndex)) {

//				LaunchMissile ();

			}

		}

	}





	void FireControlEvent(bool isTriggerPulled) {
	
		// Firing until mag automatically reloads.
		if (mag > 0) {

			if (isTriggerPulled && spawnProperties.enableFiring) {

				if ((firingMode == "automatic") || (firingMode == "semi-automatic")) {

					PullTrigger ();

				} else if (firingMode == "lock-on") {
					
//					LaunchMissile ();

				}

			} else if (!isTriggerPulled && spawnProperties.enableFiring) {

				KeyboardButtons ();

			}

		}

	}





	/** Event trigger to check when will reload ammo. */
	void MagCheck() {
	
		if((reload > 0) && isReloading) {

			reload -= Time.deltaTime;

			if ((reload <= 0) && (noOfRounds > 0)) {

				mag = roundsPerMag;

				if (noOfRounds >= fired) {

					noOfRounds -= fired;

				} else {
				
					mag = noOfRounds;
					noOfRounds = 0;

				}

				fired = 0; // --> Reset fire count to the next mag.
				isReloading = false;

			}

		}

		// Mag reloads upon a key is pressed.
		if(Input.GetKeyDown(KeyCode.R)) {

			if ((roundsPerMag < noOfRounds) && !isReloading && (fired >= 1)) {

				reload = time.reloadTime;
				isReloading = true;

			}

		}

	}





	void PullTrigger() {
	
//		// Run timer.
//		time.startFire = Time.time;

		// Fire bullet per no. of seconds.
		if(time.startFire > time.endFire) {

			// Update hit ratio.
			HitRate.RaiseShotCount();

			// Check if this direction is heading left or right.
			direction = Random.Range(-1, 2);

			if (direction == 0)
				direction = 1;

			// Setting up shot direction via angle...
			minAngle = averageAccuracyAngle * (1 - minAccuracy);
			maxAngle = averageAccuracyAngle * (1 - maxAccuracy);

			// If min and max accuracy are equal, aiming at direction at complete straight. 
			if (minAccuracy == maxAccuracy) {
			
				finalAccuracyAngle = 0;

			} else {
				
				finalAccuracyAngle = Random.Range (maxAngle, minAngle + 1);

			}

			// Set aiming position.
			finalAccuracyAngle = finalAccuracyAngle * direction;
			spawnProperties.startPoint.Rotate(0, finalAccuracyAngle, 0);

			// Updating spawned shot position + accuracy...
			yourProjectile = Instantiate(spawnProperties.ammo[0]) as GameObject;

			if(GetComponent<Move>().IsGoingLeft()) {

				yourProjectile.transform.position = new Vector3(spawnProperties.startPoint.position.x - spawnProperties.shotOffsetX,
																spawnProperties.startPoint.position.y,
																spawnProperties.startPoint.position.z);

			} else if(GetComponent<Move>().IsGoingRight()) {

				yourProjectile.transform.position = new Vector3(spawnProperties.startPoint.position.x + spawnProperties.shotOffsetX,
																spawnProperties.startPoint.position.y,
																spawnProperties.startPoint.position.z);

			}

			// Spawn the muzzle flash!
//			spawnProperties.muzzleFlash.transform.position = new Vector3(spawnProperties.startPoint.position.x + spawnProperties.shotOffsetX[0],
//																		 spawnProperties.startPoint.position.y + spawnProperties.shotOffsetY[0],
//																		 spawnProperties.startPoint.position.z);

			// Updating shot direction according to the weapon's hit rate.
			if(GetComponent<Move>().IsGoingLeft()) {
			
				yourProjectile.GetComponent<BulletBehavior> ().physics.firingVelocity *= -1;

			} else if(GetComponent<Move>().IsGoingRight()) {

				yourProjectile.GetComponent<BulletBehavior> ().physics.firingVelocity *= 1;

			}
//			yourProjectile.transform.LookAt(spawnProperties.shotDirection.transform.position);
			//yourProjectile.transform.LookAt(GameObject.FindGameObjectWithTag("Shot Direction").transform.position);


			// Play shot sound.
//			if(fireSfx != null) fireSfx.Play ();

			// Reset the aim position of the gun to default.
//			spawnProperties.startPoint.localRotation = Quaternion.Euler (0, 0, 0);

			// Consume ammo.
			mag--;
			fired++; // --> Tells how many ammo fired will be deduction to the remaining ammo in reserve.
			reload = time.reloadTime;

			// Set the next shot according to schedule.
			time.endFire = time.firingInterval + Time.time;
			time.startFire = Time.time;

		}

		// For flagging the HUD of the ammo count...
		if (mag <= 0) {
		
			isReloading = true;
//			behaviorSfx.source.PlayOneShot (behaviorSfx.reloadSound);

			if (noOfRounds <= 0) {
			
				isReloading = false;
//				behaviorSfx.source.PlayOneShot (behaviorSfx.ammoOutSound);

			}

		}

	}





//	void LaunchMissile() {
//
//		// Run timer.
//		time.startFire = Time.time;
//
//		// Fire bullet per no. of seconds.
//		if(time.startFire > time.endFire) {
//
//			// Update hit ratio.
//			HitRate.RaiseShotCount();
//
//			// Check if this direction is heading left or right.
//			direction = Random.Range(-1, 2);
//
//			if (direction == 0)
//				direction = 1;
//
//			// Setting up shot direction via angle...
//			minAngle = averageAccuracyAngle * (1 - minAccuracy);
//			maxAngle = averageAccuracyAngle * (1 - maxAccuracy);
//
//			// If min and max accuracy are equal, aiming at direction at complete straight. 
//			if (minAccuracy == maxAccuracy) {
//
//				finalAccuracyAngle = 0;
//
//			} else {
//
//				finalAccuracyAngle = Random.Range (maxAngle, minAngle + 1);
//
//			}
//
//			// Identify the target confirmed before starting to launch the missile.
//			if (range.IsEnemyInSight ()) {
//
//				// Set aiming position.
//				finalAccuracyAngle = finalAccuracyAngle * direction;
//				spawnProperties.startPoint.Rotate (0, finalAccuracyAngle, 0);
//
//				// Updating spawned shot position + accuracy...
//				yourProjectile = Instantiate (spawnProperties.ammo [0]) as GameObject;
//				yourProjectile.transform.position = new Vector3 (spawnProperties.startPoint.position.x + spawnProperties.shotOffsetX [0],
//																 spawnProperties.startPoint.position.y + spawnProperties.shotOffsetY [0],
//																 spawnProperties.startPoint.position.z);
//
//				// Spawn the muzzle flash!
//				spawnProperties.muzzleFlash.transform.position = new Vector3(spawnProperties.startPoint.position.x + spawnProperties.shotOffsetX[0],
//																			 spawnProperties.startPoint.position.y + spawnProperties.shotOffsetY[0],
//																			 spawnProperties.startPoint.position.z);
//
//				// Updating shot direction according to the weapon's hit rate and then fire missile to the selected target.
//				yourProjectile.transform.LookAt (spawnProperties.shotDirection.transform.position);
//				yourProjectile.GetComponent<BulletBehavior> ().missile.selectedTarget = range.GetTargetMech ();
//
//				// Play shot sound.
//				if (fireSfx != null)
//					fireSfx.Play ();
//
//				// Reset the aim position of the gun to default.
//				spawnProperties.startPoint.localRotation = Quaternion.Euler (0, 0, 0);
//
//				// Consume ammo count.
//				mag--;
//
//			} else { // --> Ensures lock-on is cancelled.
//			
//				// Updating spawned shot position + accuracy...
//				yourProjectile = Instantiate(spawnProperties.ammo[0]) as GameObject;
//				yourProjectile.transform.position = new Vector3(spawnProperties.startPoint.position.x + spawnProperties.shotOffsetX[0],
//																spawnProperties.startPoint.position.y + spawnProperties.shotOffsetY[0],
//																spawnProperties.startPoint.position.z);
//
//				// Updating shot direction according to the weapon's hit rate and then fire missile to the selected target.
//				yourProjectile.transform.LookAt(spawnProperties.shotDirection.transform.position);
//				yourProjectile.GetComponent<BulletBehavior> ().missile.selectedTarget = null;
//
//			}
//
//			// Set the next shot according to schedule.
//			time.endFire = time.firingInterval + Time.time;
//			time.startFire = Time.time;
//
//		}
//
//	}





	void OnTriggerEnter2D(Collider2D col) {
	
		if (hitCollision.enableHitCollision) {
		
			if (col.gameObject.CompareTag ("Target") && col.gameObject.name.Equals (hitCollision.targetName)) {

				behaviorSfx.source.PlayOneShot (behaviorSfx.punchSound);
			
				if (hitCollision.setAsPit) {

					col.gameObject.GetComponent<HealthDisplay> ().amount -= hitCollision.damage;
				
				} else {
				
					move.AI.pause = hitCollision.stopFollow; // --> AI will not move while attacking if enabled;
					col.gameObject.GetComponent<HealthDisplay> ().amount -= hitCollision.damage;
					Score.Deduct (hitCollision.scoreCost);

					if (transform.position.x > col.transform.position.x) { // --> Target at left

						col.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-hitCollision.impact, hitCollision.impact/2);
						col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-hitCollision.impact, hitCollision.impact / 2), ForceMode2D.Impulse);

					} else if (transform.position.x < col.transform.position.x) { // --> Target at right

						col.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (hitCollision.impact, hitCollision.impact/2);
						col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (hitCollision.impact, hitCollision.impact / 2), ForceMode2D.Impulse);

					} 

				}

			}

		}

	}





	void OnTriggerStay2D(Collider2D col) {

		// Playing attack animation here if available.
		if (hitCollision.enableHitCollision) {

			if (col.gameObject.CompareTag ("Target") && col.gameObject.name.Equals (hitCollision.targetName)) {

				try {

					hitCollision.animator.SetBool (hitCollision.attackName, true);

				} catch (MissingComponentException) {

				} catch(MissingReferenceException) {

				} catch(System.NullReferenceException) {

				}

			}

		}

	}





	void OnTriggerExit2D(Collider2D col) {

		try {

			move.AI.pause = false;

		} catch (MissingComponentException) {

		} catch(MissingReferenceException) {

		} catch(System.NullReferenceException) {

		}
	
		// attack animation
		try {

			hitCollision.animator.SetBool (hitCollision.attackName, false);

		} catch (MissingComponentException) {

		} catch(MissingReferenceException) {

		} catch(System.NullReferenceException) {
			
		}

	}





}
