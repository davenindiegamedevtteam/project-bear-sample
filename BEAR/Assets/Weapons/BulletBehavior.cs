﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class PhysicsBehavior {

	/** Either SOLID or ENERGY. (Case sensitive) */
	public string ammoType = "SOLID";

	public float firingVelocity = 20;
	public float elevationVelocity = 0;
	public float heat = 0; // --> Attack heat. Every shot increases temperature of the enemy target.
	public float stunPower = 0; // --> Stability Counter
	public float penetration = 0;

}





[System.Serializable]
public class MissileBehavior {

	public bool enableLockOnSelectedTarget = false;

	/** Countdown time (in sec) before a missile will follow to the selected target */
	public float lockOnDelay;

	/** The rate on how the missile reacts when following. */
	public float reactionSpeed = 0;

	public GameObject selectedTarget;

	/**
	 * 
	 * Where "missile" as argument is the selected missile you
	 * want to follow the target.
	 * 
	 */
	public void FollowTarget(GameObject missile) {
	
		if (lockOnDelay <= 0) {

			// Make sure this target selected is present. (try-catch exception)
			if(selectedTarget != null) {

				var rotation = Quaternion.LookRotation (selectedTarget.transform.position - missile.transform.position);
				missile.transform.rotation = Quaternion.Slerp (missile.transform.rotation, rotation, Time.deltaTime * reactionSpeed);

			}
		
		} else {
		
			lockOnDelay -= Time.deltaTime;

		}

	}

}





public class BulletBehavior : MonoBehaviour {





	// Bullet Properties
	public PhysicsBehavior physics;
	public float damage = 30;
	private float r, g, b, alpha; // --> Shot Colors
	public SpriteRenderer bullet;
	public TrailRenderer bulletTrail;
	public bool isHit = false;
	public bool isPenetrable = false;
	public bool showDebugLog = false;
	public bool isPenetrationFailed = false;

	// For missiles...
	public MissileBehavior missile;

	// Distance Limit
	public float rangeLimit = 0; // --> In meters by default

	// Explosion Mark
	public GameObject explosion;
	private GameObject yourExlpsion;





	void Start() {

		alpha = 1;

		try {

			r = bullet.color.r;
			g = bullet.color.g;
			b = bullet.color.b;
			
		} catch(UnassignedReferenceException e) {

			if(showDebugLog) print ("[BULLET BEHAVIOR] --> No color found in bullet sprite and/or " +
				   					"trails at the start. Switching to default 1.");

			r = 1;
			g = 1;
			b = 1;
		
		}

	}





	void FixedUpdate () {

		// Move projectile. NOTE: In 2D or 2.5D game, accuracy of the gun will be base on behavior's chance to hit instead of the bullet go offcourse similar in the FPS.
		transform.Translate(physics.firingVelocity, 0, 0);

		// If this type of weapon is a missile...
		if (missile.enableLockOnSelectedTarget) {

			if (missile.selectedTarget != null) {

				missile.FollowTarget (this.gameObject);

			} else {

				Destroy (this.gameObject);

			}

		}

		// Scrap
		if(isHit) {

			if (!isPenetrable) {
			
				if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Bullet discarded.");
				Destroy (this.gameObject);

			} else if(isPenetrationFailed && isPenetrable) {

				if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Bullet discarded. End of penetration.");
				Destroy (this.gameObject);

			}

		}

		// Fade bullet at certain range distance. (Only player ships.)
//		if(transform.position.z >= -0.5f) {
//
//			if(showDebugLog) print ("Bullet reached distance limit.");
//			Destroy (gameObject);
//
//		}

	
	}





	void OnTriggerEnter(Collider col) {

		// Register a hit.
		if (col.gameObject.CompareTag ("Robot")) {

			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Enemy target status: Hit (" + col.gameObject.name + ")");
			isHit = true;

			// Calculate damage taken.
//			MechProperties mech = col.gameObject.GetComponent<MechProperties> ();
//			if (mech != null)
//				mech.ReduceAP (damage, physics.penetration, physics.ammoType);

			// Mark an explosion.
			yourExlpsion = Instantiate (explosion) as GameObject;
			yourExlpsion.transform.position = new Vector3 (transform.position.x - 1, 
														   transform.position.y, 
				   										   transform.position.z);
			yourExlpsion.name = "Mech Hit: " + col.gameObject.name;

			// Update hit ratio.
			HitRate.RaiseHitCount ();

		} else if(col.gameObject.CompareTag ("Props") || (col.gameObject.CompareTag ("Platform"))) {
		
			// Mark an explosion.
			yourExlpsion = Instantiate (explosion) as GameObject;
			yourExlpsion.transform.position = new Vector3 (transform.position.x - 1, 
														   transform.position.y, 
														   transform.position.z);
			yourExlpsion.name = "Mech Hit on non-target: " + col.gameObject.name;

		}

	}





	void OnTriggerEnter2D(Collider2D col) {

		// Register a hit.
		if(col.gameObject.CompareTag ("Props") || (col.gameObject.CompareTag ("Platform"))) {

			// Mark an explosion.
//			yourExlpsion = Instantiate (explosion) as GameObject;
//			yourExlpsion.transform.position = new Vector3 (transform.position.x - 1, 
//				transform.position.y, 
//				transform.position.z);
//			yourExlpsion.name = "Hit on non-target: " + col.gameObject.name;

			Destroy (this.gameObject);

		} if (col.gameObject.CompareTag ("Target")) {

			if (col.gameObject.GetComponent<HealthDisplay> ().amount > 0) {
			
				col.gameObject.GetComponent<HealthDisplay> ().amount -= damage;

			} else {
			
				Score.Add (100);
				Destroy (col.gameObject);

			}
		
		}

	}





	void OnTriggerExit2D(Collider2D col) {

		// Identify first whether it is your mech or the enemy mech. Penetration damage
		// effect applied to both player and AI bmech when fires anti-material rounds or
		// rounds do capable of penetrating.
		if (col.gameObject.CompareTag ("Target")) {
		
			// Check status.
			if (showDebugLog) print ("[ BULLET BEHAVIOR ] ---> Projectile exiting from the mech...");

			// Summon the behavior component.
//			MechProperties mech = col.gameObject.GetComponent<MechProperties> ();
//
//			// Projectile will penetrate only if a mech is destroyed...
//			if (!mech.defenses.isDestroyed) {
//
//				physics.firingVelocity = 0;
//				isPenetrationFailed = true;
//
//			}

			// Or, as long as this type of bullet can penetrate it or not.
			if (!isPenetrable) {
			
				physics.firingVelocity = 0;
				Destroy (this.gameObject);
			
			} else if (isPenetrable) {

				// Update hit ratio.
				HitRate.RaiseShotCount ();
			
			}

		}

	}





	void OnBecameInvisible() {

		// Bullets gone when off the screen.
//		if(enemyBullet) {
//
//			Ship.EnemyBulletCount++;
//			print ("[ BULLET BEHAVIOR ] ---> Bullet gone off screen. " + Ship.EnemyBulletCount);
//
//		}

		Destroy (gameObject);
		
	}





}
