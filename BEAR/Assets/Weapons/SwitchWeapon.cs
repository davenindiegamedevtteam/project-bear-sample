﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class SwitchWeapon : MonoBehaviour {





	public ArmWeaponProperties[] weapons; // --> NOTE: first weapon always on arms next to back weapon.
	public GameObject[] shotDirections;
	public KeyCode switchKey;
	public AudioSource selectSFX;
	public Text ammoLabel;
	private int index;

	[Tooltip("Specifies the name of the virtual button involved in switching weapon")]
	public string buttonName = "HIT";





	void Start () {

		index = 0;

		try {

			for(int i = 1; i < weapons.Length; i++) {

				shotDirections [i].SetActive (false);

			}

		} catch(IndexOutOfRangeException) {

		}
		
	}





	void Update () {

		if (Input.GetKeyDown (switchKey) || CrossPlatformInputManager.GetButtonDown(buttonName)) {

			try {

				Change ();
				selectSFX.Play ();

			} catch(IndexOutOfRangeException) {

				// Enable next weapon selected.
				index = 0;
				weapons [index].spawnProperties.enableFiring = true;

				try {

					shotDirections [index].SetActive (true);

				} catch(IndexOutOfRangeException) {
				
				}
			
			} catch(UnassignedReferenceException) {
			
			}

		}

		// Displaying ammo count...
		ammoLabel.text = weapons[index].ammo;
		
	}





	void Change() {

		if (weapons [index].spawnProperties.enableFiring) {
		
			// Close previous weapon.
			weapons [index].spawnProperties.enableFiring = false;
			shotDirections [index].SetActive (false);

			// Switching weapon index...
			if (index < weapons.Length) {
			
				index++;

				if (index >= weapons.Length) {
				
					index = 0;

				}

			}

			// Enable next weapon selected.
			weapons [index].spawnProperties.enableFiring = true;
			shotDirections [index].SetActive (true);

		}
		
	}





}
