﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[System.Serializable]
public class MuzzleColor {

	public float r = 0;
	public float g = 0;
	public float b = 0;
	public float a = 0;

}

public class MuzzleFlashController : MonoBehaviour {





	// Fire Rate per Second
	public float fireRate = 0;
	public float startFire = 0;
	public float endFire = 0;
	public float shotOffsetX = 0;
	public float shotOffsetY = 0;
	public float shotOffsetZ = 0;
	public int shots = 0;

	// Muzzle
	public GameObject muzzleFlash;
	private GameObject yourMuzzleFlash;
	public MuzzleColor muzzlecolor;
	public string firingMode = "automatic";

	// Misc
	public bool showDebugLog = false;





	void Update () {

//		if (!MechProperties.IsShipDestroyed ()) {

			KeyboardButtons ();

//		}

	}





	void FixedUpdate() {

		// Placed it here for touch buttons.
		bool isTriggerPulled = false;

		// Check the type of firing.
		if(firingMode == "automatic") {

			isTriggerPulled = CrossPlatformInputManager.GetButton("Fire");

		} else if(firingMode == "semi-automatic") {

			isTriggerPulled = CrossPlatformInputManager.GetButtonDown("Fire");

		}

		// Check if a player tapped FIRE button.
//		if(isTriggerPulled && !ShipBehavior.IsShipDestroyed ()) {
//
//			Release ();
//
//		}
		if(isTriggerPulled) {

			Release ();

		}

	}





	void KeyboardButtons(){

		if (firingMode == "automatic") {

			if (Input.GetKey (KeyCode.J)) {

				Release ();

			}

		} else if (firingMode == "semi-automatic") {

			if (Input.GetKeyDown (KeyCode.J)) {

				Release ();

			}

		}

	}





	void Release() {

		// Run timer.
		startFire = Time.time;

		// Fire bullet per no. of seconds.
		if(startFire > endFire) {

			shots += 1;

			if(showDebugLog) {

				print("[ MUZZLE FLASH CONTROLLER ] ---> No. of shots per second: " + 
					  shots + " bullet(s)/shell(s) at " + startFire + "second(s)");

			}

			yourMuzzleFlash = Instantiate(muzzleFlash) as GameObject;
			yourMuzzleFlash.transform.position = new Vector3(transform.position.x + shotOffsetX,
															 transform.position.y + shotOffsetY,
															 transform.position.z + shotOffsetZ);
//			yourMuzzleFlash.transform.rotation = gameObject.transform.rotation;

			SpriteRenderer[] flash = yourMuzzleFlash.GetComponents<SpriteRenderer>();
			flash [0].color = new Color (muzzlecolor.r, muzzlecolor.g, muzzlecolor.b, muzzlecolor.a);

			yourMuzzleFlash.transform.parent = gameObject.transform;

			endFire = fireRate + Time.time;
			startFire = Time.time;

		}

	}





}
