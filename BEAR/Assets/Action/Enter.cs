﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enter : MonoBehaviour {





	public string sceneName = "";
	public SwitchArea area;

	public bool enableRandomArea = false;
	public bool setAsPit = false;

	[Tooltip("Enter either \"Surface\",\"Ground\", or \"Air\". (Case Sensitive)")]
	public string areaType = "Surface";

	[Tooltip("Set max range to 0 to randomize form the very first area to the last one available.")]
	public int minRange, maxRange;

	private static bool pit;





	void Start () {
		
		
	}





	void Update () {
		
	}





	void OnTriggerEnter2D(Collider2D col) {
	
		if (col.gameObject.CompareTag ("Target")) {

			if (col.gameObject.name != "Player") {
			
				Destroy (col.gameObject);
				return;

			}
		
			if (enableRandomArea) {
			
				if (areaType == "Surface") {

					if (maxRange <= 0) {
					
						area.SetSurfaceArea ();

					} else {
					
						area.SetSurfaceArea (minRange, maxRange);

					}
				
				} else if (areaType == "Ground") {

					if (maxRange <= 0) {

						area.SetGroundArea ();

					} else {

						area.SetGroundArea (minRange, maxRange);

					}
				
				} else if (areaType == "Air") {
				
				}

			} else {
			
				// Life deducted when you fall off.
				if (setAsPit) {
				
					GameStatusEvent.life -= 1;
					pit = true;

					// game over! No restart same area.
					if (GameStatusEvent.life <= 0) {

						return;

					}

				} else {
				
					GameStatusEvent.life += 1; // --> No life deduction if entered to the next area.

				}

				SceneManager.LoadScene (sceneName);

			}

		}

	}





	public static bool IsFellOffThePit () {

		return pit;

	}





	public static void VoidPit () {

		pit = false;

	}





}
