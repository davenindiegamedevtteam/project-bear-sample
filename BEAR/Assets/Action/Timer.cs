﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {





	public float min = 0;
	public float sec = 0;

	public Text time;
	private string m, s;

	private static bool stopped;





	void Start () {

		stopped = false;

		if (sec >= 60) {
		
			sec = 59;

		}
		
	}





	void FixedUpdate () {

		sec -= Time.deltaTime;

		if (sec <= -1f) {

			if (min > 0) {
			
				sec = 59;
				min--;

			} else {

				sec = 0;
				stopped = true;

			}

		}

		if (sec < 10) {
		
			s = "0" + (int)sec;

		} else {
		
			s = (int)sec + "";

		}

		if (min < 10) {

			m = "0" + min;

		} else {

			m = min.ToString ();

		}

		time.text = m + ":" + s;
		
	}





	public static bool IsStopped () {

		return stopped;

	}





}
