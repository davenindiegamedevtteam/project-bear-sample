﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarnScore : MonoBehaviour {

	public float amount = 1000;

	public void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.name == "Player") {

			Score.Add (amount);
			Destroy (this.gameObject);

		}

	}

}
