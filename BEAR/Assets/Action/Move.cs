﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[Serializable]
public class Speed {

	public float x, y, z;

}





[Serializable]
public class ViewTransition {

	public float offsetX = 0;
	public float offsetY = 0;
	public float speedX = 0;
	public float speedY1 = 0;
	public float speedY2 = 0;

	public Transform cam;

}





[Serializable]
public class KeyboardKeys {

	public KeyCode left, right;

}





[Serializable]
public class ComSettings {

	public bool enable = false;
	public GameObject target;

	[Tooltip("Checks the closeness between the player and AI player. Useful to time when to attack.")]
	public float distance = 0;

	[Tooltip("Only if within the distance range.")]
	public bool followTarget = false;

	[Tooltip("Useful when AI is attacking without moving, correcting according to animation.")]
	public bool pause = false;

	[Tooltip("Will this AI follow only within the ground?")]
	public bool followHorizontalOnly = false;

	[Tooltip("AI moves itself when out of sight.")]
	public SelfMoveSettings selfMove;

	[Tooltip("Needed to name the animation for running. (keyword)")]
	public string runName = "IsRunning";

}





[Serializable]
public class SelfMoveSettings {

	public float minTime = 0;
	public float maxTime = 0;
	public float stopTime = 0;

	public float st = 0;
	public float pause = 0;
	private bool isRight = false;

	public void Transition(Speed speed, ComSettings comSettings, GameObject host, SpriteRenderer sprite, Animator anim) {

		if (st >= stopTime) {

			// AI will stop moving...
			anim.SetBool(comSettings.runName, false);

			pause -= Time.deltaTime;

			if (pause <= 0) {
			
				if (isRight) {
				
					isRight = false;

				} else {
				
					isRight = true;

				}

				st = 0;

			}
		
		} else {

			// Running stop time...
			st += Time.deltaTime;
			pause = UnityEngine.Random.Range (minTime, maxTime);
		
			// Playing run animation...
			anim.SetBool(comSettings.runName, true);

			// Running in place...
			if (isRight) {

				sprite.flipX = false;
				host.transform.Translate (speed.x, speed.y, 0);

			} else if (!isRight) {

				sprite.flipX = true;
				host.transform.Translate (-speed.x, speed.y, 0);

			}

		}

	}
	
}





public class Move : MonoBehaviour {





	public Speed speed;
	public KeyboardKeys key;
	public ComSettings AI;

	[Tooltip("Make sure you put the cam with the tag \"MainCamera\" as the child of the playable sprite. Purpose especially in 2D is to make nice cam transition when moving along X-axis.")]
	public ViewTransition camTransition;

	private string direction;
	private float sx; // --> Cam X in transition
	private float cy, y; // --> Cam Y

	public AudioSource source;
	public AudioClip moveSfx;

	[Tooltip("Gonna need it to ensure correct running sfx when on ground.")]
	public Step step;

	private bool isMoving;
	private Vector2 lastPos;
	public float spd = 0;





	void Start () {

		lastPos = Vector2.zero;

		sx = camTransition.offsetX;
		cy = camTransition.offsetY;
		y = cy - camTransition.offsetY;

		try {

			camTransition.cam.transform.localPosition = new Vector3 (sx, camTransition.offsetY, -10);

		} catch(NullReferenceException) {

		}

		isMoving = false;

	}





	void Update () {

		if (HealthDisplay.IsDead () || Timer.IsStopped ()) {
		
			return;

		}

		if (AI.enable) {

			ComCommands ();
		
		} else {
		
			KeyboardCommands ();

		}
		
	}





	void FixedUpdate () {
	
		// Cam Transition (Sideways)
		if (direction == "left") {
		
			if (sx > -camTransition.offsetX) {
			
				sx -= camTransition.speedX;

			} else {
			
				sx = -camTransition.offsetX;

			}

		} else if (direction == "right") {

			if (sx < camTransition.offsetX) {

				sx += camTransition.speedX;

			} else {

				sx = camTransition.offsetX;

			}

		}

		// Cam Transition (Vertical)
		try {

			if (step.HasLanded ()) {

				if (cy < camTransition.offsetY) {

					cy += camTransition.speedY1;

				} else {

					cy = camTransition.offsetY;

				}

			} else {

				if (cy > y) {

					cy -= camTransition.speedY1;

				}

			}

		} catch(NullReferenceException) {
		
		}

		try {

			camTransition.cam.transform.localPosition = new Vector3 (sx, cy, -10);

		} catch(NullReferenceException) {
		
		}

	}





	void KeyboardCommands() {

		// For character's speed movement
		spd = (GetComponent<Rigidbody2D>().position - lastPos).magnitude;
		lastPos = GetComponent<Rigidbody2D>().position;
	
		// Movement keys
		if (Input.GetKey (key.left) || (CrossPlatformInputManager.GetAxis("Horizontal") < 0)) {

			isMoving = true;

			if (!source.isPlaying && step.HasLanded() && (source != null)) {
			
				source.PlayOneShot (moveSfx);

			}

			transform.Translate (-speed.x, 0, 0);
			direction = "left";
			GetComponent<SpriteRenderer> ().flipX = true; // --> For correction, make sprite via Photoshop facing right.
			GetComponent<Animator> ().SetBool ("IsRunning", true);

		} else if (Input.GetKey (key.right) || (CrossPlatformInputManager.GetAxis("Horizontal") > 0)) {

			isMoving = true;

			if (!source.isPlaying && step.HasLanded() && (source != null)) {

				source.PlayOneShot (moveSfx);

			}

			transform.Translate (speed.x, 0, 0);
			direction = "right";
			GetComponent<SpriteRenderer> ().flipX = false;
			GetComponent<Animator> ().SetBool ("IsRunning", true);

		} else {
		
			isMoving = false;
			GetComponent<Animator> ().SetBool ("IsRunning", false);

		}

	}





	void ComCommands() {

		// Computing distance between an AI player and a player.
		var d = Vector3.Distance (AI.target.transform.position, gameObject.transform.position);

		// AI move randomly when out of sight.
		AI.selfMove.Transition(speed, AI, gameObject, GetComponent<SpriteRenderer>(), GetComponent<Animator>());

		// If an AI got a target within range...
		if (d < AI.distance) {

			// Follow target if enabled.
			if (AI.followTarget) {
				
				// Follow only if this AI is programmed only on ground.
				if (AI.followHorizontalOnly) {
				
					if (AI.target.transform.position.y >= gameObject.transform.position.y) {
					
						return;

					}

				}

				isMoving = true;

				// Stalk target now!
				if (AI.target.transform.position.x > gameObject.transform.position.x) {
				
					GetComponent<Animator> ().SetBool (AI.runName, true);
					GetComponent<SpriteRenderer> ().flipX = false;
					if (!AI.pause) transform.Translate (speed.x, 0, 0); // TODO --> For bat movement, nned Y somehow...

				} else if (AI.target.transform.position.x < gameObject.transform.position.x) {

					GetComponent<Animator> ().SetBool (AI.runName, true);
					GetComponent<SpriteRenderer> ().flipX = true;
					if (!AI.pause) transform.Translate (-speed.x, 0, 0);

				}

			} else {
			
				isMoving = false;

			}
		
		}
	
	}





	public bool IsGoingLeft() {
	
		if (direction == "left") {
		
			return true;

		} else {
		
			return false;

		}

	}





	public bool IsGoingRight() {

		if (direction == "right") {

			return true;

		} else {

			return false;

		}

	}





	public bool IsItMoving() {

		// Checking if the player is still moving in place, considered as "not moving."
		if ((spd <= 0.027f) && isMoving) { // TODO --> for now, this is the stop movement for parallax.

			print ("MOVE IN PLACE " + spd);

			return false;

		} else if (isMoving) {
		
			print ("IT'S MOVING! " + spd);

		} else {
		
			print ("STOPPED. " + spd);

		}
	
		return isMoving;

	}





}
