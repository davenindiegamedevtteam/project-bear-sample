﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour {





	public bool isLanded;
	public AudioSource source;
	public AudioClip landSfx;





	void Start () {

		isLanded = false;
		
	}





	void Update () {
		
	}





	void OnTriggerEnter2D(Collider2D col) {
		
		try {

			source.PlayOneShot (landSfx);

		} catch(NullReferenceException) {
		
		}

	}





	void OnTriggerStay2D(Collider2D col) {
	
		isLanded = true;

	}





	void OnTriggerExit2D(Collider2D col) {
	
		isLanded = false;

	}





	public bool HasLanded() {

		return isLanded;
	
	}





}
