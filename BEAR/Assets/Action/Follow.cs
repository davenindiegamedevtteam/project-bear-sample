﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Intended for parallaxing fixtures for more realistic background move distance speed along X-axis. */
public class Follow : MonoBehaviour {





	/** The target you want to follow. */
	public Transform target;
	private Vector3 pos;





	void Start () {

		pos = transform.position;

	}





	void FixedUpdate () {

		transform.position = new Vector3 (target.position.x, pos.y, pos.z);
		
	}





}
