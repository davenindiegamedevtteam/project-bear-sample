﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreHealth : MonoBehaviour {

	public float amount = 25;

	public void OnTriggerEnter2D (Collider2D col) {
	
		if (col.gameObject.name == "Player") {
		
			col.gameObject.GetComponent<HealthDisplay> ().Heal (amount);
			Destroy (this.gameObject);

		}

	}

}
