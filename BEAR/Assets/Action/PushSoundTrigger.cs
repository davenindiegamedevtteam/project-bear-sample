﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushSoundTrigger : MonoBehaviour {





	public AudioSource sound;





	void OnTriggerStay2D(Collider2D col) {
	
		try {

			if (col.gameObject.GetComponent<Move> ().IsItMoving ()) {

				if (!sound.isPlaying) {

					sound.Play ();

				}

			} else {

				sound.Stop ();

			}

		} catch (NullReferenceException) {
		
		}

	}





}
