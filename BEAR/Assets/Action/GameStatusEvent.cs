﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStatusEvent : MonoBehaviour {





	public float count = 3;
	public string scene;

	public Text lifeLabel;
	public Text gameOverLabel;
	public static int life = 4;

	public string go;





	void Start () {

		// No double life deduction if player died of pitfall before. 
		if (Enter.IsFellOffThePit ()) {
		
			Enter.VoidPit ();

		} else {
		
			life--;

		}

		go = " ";

	}





	void Update () {

		try {

			lifeLabel.text = "LIFE: " + life;
			gameOverLabel.text = go;

		} catch(System.NullReferenceException) {

		}
	
	}





	void FixedUpdate () {

		// game over if fell
		if (Enter.IsFellOffThePit () && (count <= 0)) {

			go = "G A M E O V E R";

			return;

		}

		// game over if completely dead
		if (HealthDisplay.IsDead ()) {
		
			count -= Time.deltaTime;

			if (count <= 0) {

				count = 0;

				// Show GAME OVER screen here if no. of lives are ended.
				if (life > 0) {

					Restart ();

				} else {
				
					// G A M E O V E R
					go = "G A M E O V E R";

				}

			}

		} else if (Timer.IsStopped ()) {
		
			Time.timeScale = 0;
			go = "T I M E O V E R";

		} 

	}





	public void Restart() {
	
		SceneManager.LoadScene (scene);

	}





}
