﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Jump : MonoBehaviour {





	public float height = 0;
	public KeyCode button;
	public Step step;

	public AudioSource source;
	public AudioClip jumpSfx;





	void Start () {
		
	}





	void FixedUpdate () {

		if (HealthDisplay.IsDead ()) {

			return;

		}

		// Triger jump when pressed. 
		if (Input.GetKeyDown (button) && step.HasLanded()) { // --> Keys

			source.PlayOneShot (jumpSfx);

			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, height);
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, height), ForceMode2D.Impulse);
		
		} else if (CrossPlatformInputManager.GetButtonDown ("Jump") && step.HasLanded()) { // --> Smartphones/Tablets

			source.PlayOneShot (jumpSfx);

			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, height);
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, height), ForceMode2D.Impulse);

		}

		// Jump Animation
		try {

			if (step.HasLanded ()) {

				GetComponent<Animator> ().SetBool ("IsJumping", false);

			} else if (!step.HasLanded ()) {

				GetComponent<Animator> ().SetBool ("IsJumping", true);

			}

		} catch(MissingComponentException) {
		
		}
		
	}





	public void Now() {
	
		source.PlayOneShot (jumpSfx);

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, height);
		GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, height), ForceMode2D.Impulse);

	}





}
