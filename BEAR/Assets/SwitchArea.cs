﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchArea : MonoBehaviour {





	public static int area = 5; // --> Serves as unlocker of next area as the game progresses.
	public static int noOfAreas; // --> Serves as no. of areas per section available. Initiator before the game. (USEFUL DURING EDITING/REVISING  LEVEL.)
	public static string[] areas; // --> Surface area.
	public static string[] gAreas; // --> Ground area.





	void Start () {
		
	}





	void Update () {
		
	}





	public static void BeginLevel () {
	
		noOfAreas = 1;

		areas = new string[noOfAreas];
		gAreas = new string[noOfAreas];

		for (int i = 0; i < areas.Length; i++) {
		
			areas [i] = "Area " + (i + 1);

		}

		for (int i = 0; i < gAreas.Length; i++) {

			gAreas [i] = "Ground Area " + (i + 1);

		}

	}





	public void SetSurfaceArea(int min, int max) {
	
		SceneManager.LoadScene (areas [Random.Range(min, max)]);

	}





	public void SetSurfaceArea() {

		SceneManager.LoadScene (areas [Random.Range(0, noOfAreas)]);

	}





	public void SetGroundArea(int min, int max) {

		SceneManager.LoadScene (gAreas [Random.Range(min, max)]);

	}





	public void SetGroundArea() {

		SceneManager.LoadScene (gAreas [Random.Range(0, noOfAreas)]);

	}





}
