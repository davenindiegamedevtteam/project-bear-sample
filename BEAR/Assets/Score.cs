﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {





	private static float score = 0;





	void Start () {
		
	}





	void Update () {

		GetComponent<Text> ().text = "Score: " + score;
		
	}





	public static void Deduct(float value) {

		score -= value;

		if (score < 0) {

			score = 0;

		}

	}





	public static void Add(float value) {

		score += value;

	}





	public static void RestartScore () {
	
		score = 0;

	}





}
